= Aggcheck script documentation
Luke Snyder <luke@ragriz.me>
v1.0, 2017-12-07

== Purpose
This script is designed to check known aggregations for known values. It will record all observed values,
missing values, and expected values as well as the count of each. Additionally, if more then one 
aggregation is checked it will record a grand total for all missing, observed, and expected values. This 
is desiged to work in conjunction with the link:makealert.asciidoc[makealert script]. Below are some 
example invocations of the script.

== Parameters
All possible parameters are below. Please see the Invocation section for examples of the paramaters in different forms.

=== checks:
* _required_: Yes
* _type_: Array of Maps
* _description_: Provides the information needed to check values in aggregations.

=== checks.expected:
* _required_: Yes
* _type_: Array of Strings or Arrays
* _description_: An array of the expected values. This could be (1) an array of strings, (2) it
  could be an array of arrays of strings, or (3) it could be a
  combination.

.Examples
|===
|Value|Meaning

|["8.8.8.8", "4.4.2.2"]
|8.8.8.8 AND 4.4.2.2

|[ ["8.8.8.8", "8.8.4.4"], ["4.4.4.4", "4.4.2.2"] ]
|(8.8.8.8 OR 8.8.4.4) AND ("4.4.4.4" OR "4.4.2.2")

|["8.8.8.8", ["4.4.4.4", "4.4.2.2"]]
|8.8.8.8 AND (4.4.4.4 OR 4.4.2.2)
|===

=== checks.name:
* _required_: Yes
* _type_: String


== Invocation
In all of the examples the script as been stored as aggcheck. Please use the stored scripts API to save 
this script in the cluster state.

=== Example 1
In this example we are monitoring for silent devices. Simply, the input will aggregate the unique values seen in
the host field. Then before the actions are carried out, transform the payload with aggcheck. This will create a
new search like payload to feed into the actions. The first and only action has a secondary condition. It checks if
the totalMissing value is >= 1. If the condition is true, the payload is transformed again with the 
link:makealert.asciidoc[makealert script].

----
{
  "trigger": {
    "schedule": {
      "interval": "60m"
    }
  },
  "input": {
    "search": {
      "request": {
        "search_type": "query_then_fetch",
        "indices": [
          "<logs-{now/d{YYYY.ww}}>"
        ],
        "types": [],
        "body": {
          "size": 0,
          "query": {
            "bool": {
              "filter": [
                {
                  "range": {
                    "@timestamp": {
                      "gte": "now-65m/m",
                      "lt": "now-5m/m"
                    }
                  }
                }
              ]
            }
          },
          "aggs": {
            "talkinghosts": {
              "terms": {
                "field": "host",
                "size": 100
              }
            }
          }
        }
      }
    }
  },
  "condition": {
    "always": {}
  },
  "transform": {
    "script": {
      "stored": "aggcheck",
      "params": {
        "checks": [
          {
            "expected": [
              ["10.0.0.1","10.0.0.2"],
              ["10.0.1.1","10.0.1.2"],
              "10.0.2.1",
              "10.0.3.1"
            ],
            "name": "talkinghosts"
          }
        ]
      }
    }
  }
  "actions": {
    "index_document": {
      "condition": {
        "compare": {
          "ctx.payload.hits.totalMissing": {
            "gte": 1
          }
        }
      },
      "transform": {
        "script": {
          "stored": "makealert",
          "lang": "painless",
          "params": {
            "severity": "Critical",
            "addfields": {
              "message": "No events from test devices in the last hour. Please investigate."
            },
            "fields": [
              "expected",
              "missing",
              "observed",
              "totalMissing"
            ],
            "category": "Admin"
          }
        }
      },
      "index": {
        "index": "<elastic-events-{now/d{YYYY.ww}}>",
        "doc_type": "event"
      }
    }
  },
  "metadata": {
    "description": "Aggregate total count of events from each host for a 60 minute span. If any known hosts are missing, alert."
  }
}
----
The results of this watch could looks like this.

----
{
  "watch_id": "_inlined_",
  "state": "executed",
  "_status": {
    ...
  },
  "trigger_event": {
    ...
  },
  "input": {
    "search": {
      ...
      }
    }
  },
  "condition": {
    "always": {}
  },
  "metadata": {
    "name": "Silence monitor for test devices",
    "description": "Aggregate total count of events from each host for a 60 minute span. If any known hosts are missing, alert."
  },
  "result": {
    "execution_time": "2017-12-07T21:45:49.431Z",
    "execution_duration": 10,
    "input": {
      "type": "search",
      "status": "success",
      "payload": {
        "_shards": {
          "total": 1,
          "failed": 0,
          "successful": 1
        },
        "hits": {
          "hits": [],
          "total": 225,
          "max_score": 0
        },
        "took": 8,
        "timed_out": false,
        "aggregations": {
          "talkinghosts": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 0,
            "buckets": [
              {
                "doc_count": 148,
                "key": "10.0.0.2"
              },
              {
                "doc_count": 74,
                "key": "10.0.1.1"
              },
              {
                "doc_count": 3,
                "key": "10.0.3.1"
              }
            ]
          }
        }
      },
      "search": {
        ....
    },
    "condition": {
      "type": "always",
      "status": "success",
      "met": true
    },
    "transform": {
      "type": "script",
      "status": "success",
      "payload": {
        "hits": {
          "hits": [
            {
              "_source": {
                "aggMissing": 1,
                "expected": [
                  [
                    "10.0.0.1",
                    "10.0.0.2"
                  ],
                  [
                    "10.0.1.1",
                    "10.0.1.2"
                  ],
                  "10.0.2.1",
                  "10.0.3.1"
                ],
                "aggObserved": 3,
                "missing": [
                  "10.0.2.1"
                ],
                "observed": [
                  "10.0.0.2",
                  "10.0.1.1",
                  "10.0.3.1"
                ],
                "aggExpected": 4
              }
            }
          ],
          "totalMissing": 1,
          "totalObserved": 3,
          "totalExpected": 4
        }
      }
    },
    "actions": [
      {
        "id": "index_document",
        "type": "index",
        "status": "simulated",
        "condition": {
          "type": "compare",
          "status": "success",
          "met": true,
          "compare": {
            "resolved_values": {
              "ctx.payload.hits.totalMissing": 1
            }
          }
        },
        "transform": {
          "type": "script",
          "status": "success",
          "payload": {
            "severity": "Critical",
            "totalhits": null,
            "cat": "Admin",
            "name": "Silence monitor for test devices",
            "description": "Aggregate total count of events from each host for a 60 minute span. If any known hosts are missing, alert.",
            "message": "No events from test devices in the last hour. Please investigate.",
            "events": [
              {
                "totalMissing": 1,
                "_index": "None",
                "expected": [
                  [
                    "10.0.0.1",
                    "10.0.0.2"
                  ],
                  [
                    "10.0.1.1",
                    "10.0.1.2"
                  ],
                  "10.0.2.1",
                  "10.0.3.1"
                ],
                "missing": [
                  "10.0.2.1"
                ],
                "_id": "None",
                "observed": [
                  "10.0.0.2",
                  "10.0.1.1",
                  "10.0.3.1"
                ]
              }
            ],
            "timestamp": "2017-12-07T21:45:49.431Z"
          }
        },
        "index": {
          "request": {
            "index": "<elastic-events-{now/d{YYYY.ww}}>",
            "doc_type": "event",
            "source": {
              "severity": "Critical",
              "totalhits": null,
              "cat": "Admin",
              "name": "Silence monitor for test devices",
              "description": "Aggregate total count of events from each host for a 60 minute span. If any known hosts are missing, alert.",
              "message": "No events from test devices in the last hour. Please investigate.",
              "events": [
                {
                  "totalMissing": 1,
                  "_index": "None",
                  "expected": [
                    [
                      "10.0.0.1",
                      "10.0.0.2"
                    ],
                    [
                      "10.0.1.1",
                      "10.0.1.2"
                    ],
                    "10.0.2.1",
                    "10.0.3.1"
                  ],
                  "missing": [
                    "10.0.2.1"
                  ],
                  "_id": "None",
                  "observed": [
                    "10.0.0.2",
                    "10.0.1.1",
                    "10.0.3.1"
                  ]
                }
              ],
              "timestamp": "2017-12-07T21:45:49.431Z"
            }
          }
        }
      }
    ]
  },
  "messages": []
}
----